//Maria Barba 1932657
package movies.importer;
import java.io.*;
public class ProcessingTest {
	public static void main (String [] args) throws IOException {
		LowercaseProcessor myProc = new LowercaseProcessor("C:\\maria\\courses\\java310\\lab05tests\\sourcefolder","C:\\maria\\courses\\java310\\lab05tests\\destinationfolder");
		myProc.execute();
		RemoveDuplicates myNoDuplicatesProc= new RemoveDuplicates("C:\\maria\\courses\\java310\\lab05tests\\destinationfolder","C:\\maria\\courses\\java310\\lab05tests\\noduplicatesdestinationfolder");
		myNoDuplicatesProc.execute();
	}

}
