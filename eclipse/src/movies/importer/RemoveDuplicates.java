//Maria Barba 1932657
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	
	public RemoveDuplicates(String srcDir, String outputDir) {
			super(srcDir,outputDir,false);
		}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicates = new ArrayList<String>();
		for(int i=0; i<input.size() ; i++) {
			if(!(noDuplicates.contains(input.get(i)))) {
				//if the value from the original array does not already exist in my new array
				//I will add it to the new array
				noDuplicates.add(input.get(i));
			}
			
		}
		return noDuplicates;
}
}